import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces/product';
import { Subscription } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { AuthService } from 'src/app/services/auth.service';
import { LoadingController, ToastController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public products = new Array<Product>();
  private productsSubscription: Subscription;
  private loading: any;

  constructor(
    private loadingCrtl: LoadingController,
    private toastCrtl: ToastController,
    private authService: AuthService,
    private productService: ProductService) {
    this.productsSubscription = this.productService.getProducts().subscribe(data => {
      this.products = data;
    });
  }

  ngOnInit() {
  }

  async logout() {
    try {
      await this.authService.logout()
    } catch (error) {
      console.log(error);
    }


  }

  ngOnDestroy() {
    this.productsSubscription.unsubscribe();
  }

  async deleteProduct(id: string) {
    try {
      await this.productService.delete(id);
    } catch (error) {
      this.presentToast('Erro ao tentar salvar')
    }
  }

  async presentLoading() {
    this.loading = await this.loadingCrtl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCrtl.create({ message, duration: 2000 });
    toast.present();
  }

}
