export interface Product {
    id?: string;
    name?: string;
    description?: string;
    picture?: any;
    price?: string;
    createdAt?: number;
    userId?: string;
}
